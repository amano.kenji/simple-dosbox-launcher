(import spork/argparse)
(import spork/sh)
(import spork/path)

(def- help-text
  ``
  Launch an executable in an archive file with dosbox.

  Application data is stored in $XDG_DATA_HOME/simple-dosbox-launcher/instance

  Runtime data is managed in $XDG_RUNTIME_DIR/simple-dosbox-launcher/instance

  Instance-specific dosbox configuration is found at
  $XDG_CONFIG_HOME/simple-dosbox-launcher/instance.conf

  Thus, setting instance option is important.

  The following dosbox types are supported.

  * dosbox-staging
  ``)

(defn- print-info
  [{"conf" conf "instance" instance "file" file "exe" exe "type" type}
   inst-conf lower upper merged]
  (printf "instance-specific dosbox configuration for %s:\n%s"
          instance inst-conf)
  (when conf
    (printf "Additional configuration: %s" conf))
  (when type
    (printf "Dosbox type: %s" type))
  (printf "User data for %s:\n%s" instance upper)
  (printf "%s\nwill be extracted onto\n%s" file lower)
  (printf "%s\nwill be laid on top of\n%s\nat %s"
          upper lower merged)
  (printf "\n%s/%s will be executed." merged exe)
  (print))

(defn- cleanup
  [verbose merged runtime-dir]
  (when verbose
    (print "\nCleaning up..."))
  (os/execute ["fusermount" "-u" merged] :p)
  (sh/rm runtime-dir))

(defn- launch-dosbox
  [{"file" file "conf" conf "exe" exe "type" type} inst-conf lower upper merged]
  (os/execute ["atool" "-q" "-X" lower file] :xp)
  (os/execute ["unionfs" "-o" "cow,hide_meta_files" (string/format "%s=RW:%s=RO" upper lower) merged] :xp)
  (os/execute ["dosbox" "-exit"
               ;(case type
                  "dosbox-staging"
                  []
                  # for vanilla dosbox and other types of dosbox forks
                  ["-userconf"])
               "-conf" inst-conf
               ;(if conf
                  ["-conf" conf]
                  [])
               (path/join merged exe)]
              :xp))

(defn- parse-opts
  []
  (argparse/argparse
    help-text
    "conf" {:kind :option
            :short "c"
            :help "Path to additional dosbox config file."}
    "file" {:kind :option
            :short "f"
            :required true
            :help "archive file that contains dosbox program."}
    "instance" {:kind :option
                :short "i"
                :required true
                :help "Instance name"}
    "exe" {:kind :option
           :required true
           :short "e"
           :help "Relative path to executable in archive file"}
    "type" {:kind :option
            :short "t"
            :help "the type of dosbox fork. If it is not specified, vanilla dosbox is assumed."}
    "verbose" {:kind :flag
               :short "v"
               :help "Verbose output"}))

(defn main
  [&]
  (when-let [opts (parse-opts)
             {"instance" instance "verbose" verbose} opts]
    (if-let [home (os/getenv "HOME")]
      (if-let [xdg-runtime-dir (os/getenv "XDG_RUNTIME_DIR")]
        (let [xdg-conf-dir (os/getenv "XDG_CONFIG_HOME"
                                      (path/join home ".config"))
              xdg-data-dir (os/getenv "XDG_DATA_HOME"
                                      (path/join home ".local/share"))
              conf-dir (path/join xdg-conf-dir "simple-dosbox-launcher")
              inst-conf (string (path/join conf-dir instance) ".conf")
              runtime-dir (path/join xdg-runtime-dir "simple-dosbox-launcher" instance)
              lower (path/join runtime-dir "lower")
              merged (path/join runtime-dir "merged")
              upper (path/join xdg-data-dir "simple-dosbox-launcher" instance)]
          (when verbose
            (print-info opts inst-conf lower upper merged))
          (loop [dir :in [lower upper merged]]
            (unless (sh/exists? dir)
              (sh/create-dirs dir)))
          (defer (cleanup verbose merged runtime-dir)
            (launch-dosbox opts inst-conf lower upper merged)))
        (error "Failed to get $XDG_RUNTIME_DIR"))
      (error "Failed to get $HOME"))))
